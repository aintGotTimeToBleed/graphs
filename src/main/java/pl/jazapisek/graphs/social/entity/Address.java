package pl.jazapisek.graphs.social.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Entity
@Table
@Data
public class Address implements Serializable {
    
    @Id
    private Integer id;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "street")
    private String street;
    
    @OneToOne
    @JoinColumn(name = "workplace_id", referencedColumnName = "id")
    private Workplace workplace;
    
}
