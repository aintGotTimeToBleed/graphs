package pl.jazapisek.graphs.social.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Entity
@Table(name = "friends")
@Data
public class Friends implements Serializable {

    @Id
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "facebook_id", referencedColumnName = "id")
    private Facebook facebook;
    
    @Column(name = "friend_facebook_id")
    private Integer friendFacebookId;
    
}
