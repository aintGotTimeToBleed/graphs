package pl.jazapisek.graphs.social.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Entity
@Table(name = "facebook")
@Data
public class Facebook implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "facebook_id")
    private Long facebookId;
    
    @Column(name = "facebook_name")
    private String facebookName;
    
    @OneToOne(mappedBy = "facebook")
    private Email email;
    
    @OneToMany(mappedBy = "facebook")
    private List<Workplace> workplace = new ArrayList<>();
    
}
