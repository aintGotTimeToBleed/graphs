package pl.jazapisek.graphs.social.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Entity
@Table(name = "email")
@Data
public class Email implements Serializable {
    
    @Id
    private Integer id;
    
    @Column(name = "email_address")
    private String emailAddress;
    
    @OneToOne
    @JoinColumn(name = "facebook_id", referencedColumnName = "id")
    private Facebook facebook;
    
}
