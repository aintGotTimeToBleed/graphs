package pl.jazapisek.graphs.social.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Entity
@Table(name = "workplace")
@Data
public class Workplace implements Serializable {

    @Id
    private Integer id;

    @Column(name = "name")
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "facebook_id", referencedColumnName = "id")
    private Facebook facebook;
    
    @OneToOne(mappedBy = "workplace")
    private Address address;
    
}
