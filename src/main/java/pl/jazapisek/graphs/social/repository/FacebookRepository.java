package pl.jazapisek.graphs.social.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pl.jazapisek.graphs.social.entity.Facebook;

/**
 *
 * @author mzapisek
 */
@Service
public interface FacebookRepository extends CrudRepository<Facebook, Integer> {

    List<Facebook> findByFacebookName(@Param("facebookName") String facebookName);

}
