package pl.jazapisek.graphs.controller;

import com.arangodb.entity.ArangoDBVersion;
import com.arangodb.springframework.core.ArangoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author mzapisek
 */
@Controller
public class DashboardController {

    @Autowired
    private ArangoOperations arangoOperations;
    
    @RequestMapping("/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @RequestMapping("/populate")
    @ResponseBody
    public String populate() {
        return null;
    }

    @RequestMapping("/version")
    @ResponseBody
    public String version() {
        ArangoDBVersion version = arangoOperations.getVersion();
        return version.getServer() + " " + version.getVersion();
    }

}
