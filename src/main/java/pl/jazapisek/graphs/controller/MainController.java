package pl.jazapisek.graphs.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.jazapisek.graphs.social.entity.Address;
import pl.jazapisek.graphs.social.entity.Facebook;
import pl.jazapisek.graphs.social.repository.FacebookRepository;
import pl.jazapisek.graphs.social.entity.Email;
import pl.jazapisek.graphs.social.entity.Workplace;

/**
 *
 * @author mzapisek
 */
@Controller
public class MainController {

    @Autowired
    FacebookRepository facebookRepository;

    @RequestMapping("/test")
    @ResponseBody
    public void test() {

        List<Facebook> fbJanusz = facebookRepository.findByFacebookName("Janusz");
        
        fbJanusz.stream().forEach((f) -> {
            System.out.println(f.getFacebookName() + " " + f.getEmail().getEmailAddress());
            List<Workplace> workplace = f.getWorkplace();
            workplace.stream().forEach((w) -> {
                Address address = w.getAddress();
                System.out.println(w.getName() + address.getCity());
            });
        });

    }

}
