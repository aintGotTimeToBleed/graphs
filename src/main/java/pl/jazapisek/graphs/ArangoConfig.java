package pl.jazapisek.graphs;

import com.arangodb.ArangoDB;
import com.arangodb.Protocol;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 *
 * @author mzapisek
 */
@Configuration
@EnableArangoRepositories(basePackages = {"pl.jazapisek.graphs.arango"})
public class ArangoConfig extends AbstractArangoConfiguration {

    /**
     * 
     * When using ArangoDB 3.0 it is required to set the transport protocol 
     * to HTTP and fetch the dependency - added: .useProtocol(Protocol.HTTP_JSON)
     * 
     * Credenatilans are strored i properties file. 
     * 
     * 
     * @return 
     */
    @Primary
    @Override
    public ArangoDB.Builder arango() {
        return new ArangoDB.Builder().useProtocol(Protocol.HTTP_JSON);
    }
    
    @Primary
    @Override
    public String database() {
        return "graphs";
    }
    
}
