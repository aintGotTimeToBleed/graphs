package pl.jazapisek.graphs.arango.entity;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 *
 * @author mzapisek
 */
@Edge("links")
@Data
public class Links {
    
    @Id
    private String id;
    
    @From
    private Social from;
    
    @To
    private Social to;

    public Links(String id, Social from, Social to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }
    
}
