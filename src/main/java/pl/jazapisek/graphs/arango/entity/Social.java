package pl.jazapisek.graphs.arango.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import java.util.Collection;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 *
 * @author mzapisek
 */
@Document("social")
@Data
public class Social {
    
    @Id
    private String id;
    
    private String fbId;
    private String fbName;

    @Relations(edges = Links.class, lazy = true)
    private Collection<Links> links;
    
    public Social() {
        super();
    }

    public Social(String id) {
        this.id = id;
    }

    public Social(String id, String fbId, String fbName) {
        this.id = id;
        this.fbId = fbId;
        this.fbName = fbName;
    }
    
}
