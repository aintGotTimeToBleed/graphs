package pl.jazapisek.graphs.arango.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import pl.jazapisek.graphs.arango.entity.Social;

/**
 *
 * @author mzapisek
 */
public interface SocialRepository extends ArangoRepository<Social> {
    
    Iterable<Social> findByFbId(String id);
    
}
